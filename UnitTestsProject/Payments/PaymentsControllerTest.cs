﻿using BidFlightWebAPIs.Controllers.Payment;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace UnitTestsProject
{
    public class PaymentsControllerTest
    {
        private Mock<IPaymentsRepository> repoMock;
        private PaymentController controller;

        public PaymentsControllerTest()
        {
            repoMock = new Mock<IPaymentsRepository>();
            controller = new PaymentController(repoMock.Object);
        }


        [Fact]
        public void TestGetAll()
        {
            //Arrange
            repoMock.Setup(x => x.GetPayments()).Returns(GetTestService());

            //Act
            var result = controller.GetPayments();

            //Assert
            var viewResult = Assert.IsType<List<Payments>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Payments>>(result)
;
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public void GetById_WhenIdNotFound()
        {
            //Arrange
            var mockId = 1;
            var mockPayment = new List<Payments>
            {
                new Payments()
                {
                    PaymentId = 1,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70

                },
                new Payments()
                {
                    PaymentId = 2,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70
                }
            };
            repoMock.Setup(x => x.GetPayment(mockId)).Returns(mockPayment);

            // Act
            var result = controller.GetPayments(3);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetById_WhenIdExists()
        {
            //Arrange
            var mockId = 1;
            var mockPayment = new List<Payments>
            {
                new Payments()
                {
                    PaymentId = 1,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70

                },
                new Payments()
                {
                    PaymentId = 1,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70
                }
            };

            repoMock.Setup(repo => repo.GetPayment(mockId)).Returns(mockPayment);

            // Act
            var result = controller.GetPayments(mockId) as OkObjectResult;

            //Assert
            var viewResult = Assert.IsType<List<Payments>>(result.Value);
            Assert.Equal(mockPayment, result.Value);
        }

        [Fact]
        public void TestPost_MissingFieldBillingId()
        {
            //Arrange
            var mockPayment = new Payments()
            {
                PaymentId = 1,
                BillingId = 1,
                Subtotal = 50,
                Fees = 10,
                Tax = 10,
                TotalAmount = 70
            };
            controller.ModelState.AddModelError("UserId", "Required");

            //Act
            var result = controller.PostPayments(mockPayment);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void TestPost_ValidPost()
        {
            //Arrange
            var mockPayment = new Payments()
            {
                PaymentId = 1,
                BillingId = 1,
                Subtotal = 50,
                Fees = 10,
                Tax = 10,
                TotalAmount = 70
            };
            repoMock.Setup(repo => repo.AddBillingDetails(mockPayment));

            //Act
            var response = controller.PostPayments(mockPayment);

            //Assert
            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public void TestPut_ValidUpdate()
        {
            //Arrange
            var mockId = 1;
            var mockPayment = new Payments()
            {
                PaymentId = 1,
                BillingId = 1,
                Subtotal = 50,
                Fees = 10,
                Tax = 10,
                TotalAmount = 70
            };
            repoMock.Setup(repo => repo.UpdateDetails(mockPayment,mockId));

            //Act
            var result = controller.PutPayments(mockId, mockPayment) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void TestPut_InvalidId()
        {
            //Arrange
            var mockId = 7;
            var mockPayment = new Payments()
            {
                PaymentId = 1,
                BillingId = 1,
                Subtotal = 50,
                Fees = 10,
                Tax = 10,
                TotalAmount = 70
            };
            repoMock.Setup(repo => repo.UpdateDetails(mockPayment,mockId));

            //Act
            var result = controller.PutPayments(mockId, mockPayment) as StatusCodeResult;

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void TestDelete_InValidId()
        {
            //Arrange
            var mockId = 10;
            repoMock.Setup(x => x.DeletePayment(mockId));

            //Act
            var result = controller.DeletePayments(mockId);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        private List<Payments> GetTestService()
        {
            var service = new List<Payments>
            {
                new Payments()
                {
                    PaymentId = 1,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70

                },
                new Payments()
                {
                    PaymentId = 2,
                    BillingId = 2,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70
                },
                new Payments()
                {
                    PaymentId = 3,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70
                },
                new Payments()
                {
                    PaymentId = 4,
                    BillingId = 1,
                    Subtotal = 50,
                    Fees = 10,
                    Tax = 10,
                    TotalAmount = 70
                }
            };
            return service;
        }
    }
}

﻿using BidFlightWebAPIs.Controllers.Reservations;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace UnitTestsProject.Reservations
{ 

    public class ReservationsControllerTest
    {
        private Mock<IReservationRepository> repoMock;
        private ReservationsController controller;

        public ReservationsControllerTest()
        {
            repoMock = new Mock<IReservationRepository>();
            controller = new ReservationsController(repoMock.Object);
        }

        [Fact]
        public void TestGetAll()
        {
            //Arrange
            repoMock.Setup(x => x.GetReservations()).Returns(GetTestService());

            //Act
            var result = controller.GetReservation();

            //Assert
            var viewResult = Assert.IsType<List<Reservation>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Reservation>>(result)
;
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public void GetById_WhenIdNotFound()
        {
            //Arrange
            var mockId = 1;
            var mockReservation = new List<Reservation>
            {
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "123",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"

                },
                new Reservation()
                {
                    ReservationId = 2,
                    BookingTravellerId = 1,
                    ReferenceNumber = "456",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"
                }
            };
            repoMock.Setup(x => x.GetReservation(mockId)).Returns(mockReservation);

            // Act
            var result = controller.GetReservation(5);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetById_WhenIdExists()
        {
            //Arrange
            var mockId = 1;
            var mockReservation = new List<Reservation>
            {
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "123",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"

                },
                new Reservation()
                {
                    ReservationId = 2,
                    BookingTravellerId = 1,
                    ReferenceNumber = "456",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"
                }
            };

            repoMock.Setup(repo => repo.GetReservation(mockId)).Returns(mockReservation);

            // Act
            var result = controller.GetReservation(mockId) as OkObjectResult;

            //Assert
            var viewResult = Assert.IsType<List<Reservation>>(result.Value);
            Assert.Equal(mockReservation, result.Value);
        }

        [Fact]
        public void TestPost_MissingFieldBookingTravellerId()
        {
            //Arrange
            var mockReservation = new ReservationPaymentViewModel()
            {
                NumberOfTravellers = 1,
                Status = "Pending",
                Origin = "Johannesburg",
                Destination = "Durban",
                DepartingDate = "01/01/2018",
                DepartingTime = "01/01/2018"
            };
            controller.ModelState.AddModelError("BookingTravellerId", "Required");

            //Act
            var result = controller.PostReservation(mockReservation);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void TestPost_ValidPost()
        {
            //Arrange
            var mockReservation = new ReservationPaymentViewModel()
            {
                BookingTravellerId = 1,
                NumberOfTravellers = 1,
                Status = "Pending",
                Origin = "Johannesburg",
                Destination = "Durban",
                DepartingDate = "01/01/2018",
                DepartingTime = "01/01/2018"
            };
            repoMock.Setup(repo => repo.NewReservation(mockReservation));

            //Act
            var response = controller.PostReservation(mockReservation);

            //Assert
            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public void TestPut_ValidUpdate()
        {
            //Arrange
            var mockId = 5;
            var mockReservation = new Reservation()
            {
                ReservationId = 5,
                BookingTravellerId = 5,
                ReferenceNumber = "123",
                NumberOfTravellers = 1,
                Status = "Pending",
                Origin = "Johannesburg",
                Destination = "Durban",
                DepartingDate = "01/01/2018",
                DepartingTime = "01/01/2018"
            };
            repoMock.Setup(repo => repo.UpdateResevation(mockReservation,mockId));

            //Act
            var result = controller.PutReservation(mockId, mockReservation) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void TestPut_InvalidId()
        {
            //Arrange
            var mockId = 7;
            var mockReservation = new Reservation()
            {
                ReservationId = 1,
                BookingTravellerId = 1,
                ReferenceNumber = "123",
                NumberOfTravellers = 1,
                Status = "Pending",
                Origin = "Johannesburg",
                Destination = "Durban",
                DepartingDate = "01/01/2018",
                DepartingTime = "01/01/2018"
            };
            repoMock.Setup(repo => repo.UpdateResevation(mockReservation,mockId));

            //Act
            var result = controller.PutReservation(mockId, mockReservation) as StatusCodeResult;

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        //[Fact]
        //public void TestDelete_ValidId()
        //{
        //    //Arrange
        //    var newTraveller = new Reservation()
        //    {
        //        ReservationId = 1,
        //        BookingTravellerId = 1,
        //        ReferenceNumber = "123",
        //        NumberOfTravellers = "1",
        //        Status = "Pending",
        //        Origin = "Johannesburg",
        //        Destination = "Durban",
        //        DepartingDate = "01/01/2018",
        //        ArrivalDate = "01/01/2018"
        //    };
        //    repoMock.Setup(repo => repo.AddBillingDetails(newTraveller));
        //    var controller = new BillingDetailsController(repoMock.Object);
        //    var mockId = 5;

        //    //Act
        //    var add = controller.PostBillingDetails(newTraveller);
        //    var result = controller.DeleteBillingDetails(mockId);

        //    //Assert
        //    Assert.IsType<OkObjectResult>(result);
        //}

        [Fact]
        public void TestDelete_InValidId()
        {
            //Arrange
            var mockId = 10;
            repoMock.Setup(x => x.DeleteReservation(mockId));

            //Act
            var result = controller.DeleteReservation(mockId);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        private List<Reservation> GetTestService()
        {
            var service = new List<Reservation>
            {
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "123",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"

                },
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "123",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"
                },
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "456",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"
                },
                new Reservation()
                {
                    ReservationId = 1,
                    BookingTravellerId = 1,
                    ReferenceNumber = "789",
                    NumberOfTravellers = 1,
                    Status = "Pending",
                    Origin = "Johannesburg",
                    Destination = "Durban",
                    DepartingDate = "01/01/2018",
                    DepartingTime = "01/01/2018"
                }
            };
            return service;
        }
    }
}

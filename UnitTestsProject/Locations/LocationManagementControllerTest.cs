﻿using BidFlightWebAPIs.Controllers.Locations;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.Locations;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ServiceModules.ServiceImplementaion.Locations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace UnitTestsProject.Locations
{
   public class LocationManagementControllerTest
    {
        private Mock<ILocationManagementRepository> repoMock;
        private LocationManagementController controller;

        public LocationManagementControllerTest()
        {
            repoMock = new Mock<ILocationManagementRepository>();
            controller = new LocationManagementController(repoMock.Object);
        }

        [Fact]
        public void TestGetAll()
        {
            //Arrange
            repoMock.Setup(e => e.GetLocations()).Returns(GetTestService());

            //Act
            var result = controller.GetLocations();

            //Assert
            var viewResult = Assert.IsType<List<Location>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Location>>(result)
;
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void GetByLocation()
        {
            //Arrange
            var mockSeacrh = "Durban";
            var mockLocation = new Location()
            {
                LocationId = 2,
                LocationName = "Durban",
                LocationDetails = "Durban"
            };

            repoMock.Setup(repo => repo.GetLocation(mockSeacrh)).Returns(mockLocation);

            // Act
            var result = controller.GetLocation(mockSeacrh);

            //Assert
            var viewResult = Assert.IsType<Location>(result);
            Assert.Equal(mockLocation, viewResult);
        }

        [Fact]
        public void TestPost_ValidPost()
        {
            //Arrange
            var mockLocation = new Location()
            {
                LocationId = 2,
                LocationName = "Durban",
                LocationDetails = "Durban"
            };

            repoMock.Setup(repo => repo.AddLocation(mockLocation));

            //Act
            var response = controller.PostLocation(mockLocation);

            //Assert
            Assert.IsType<CreatedAtActionResult>(response);
        }

        private List<Location> GetTestService()
        {
            var service = new List<Location>
            {
                new Location()
                {
                    LocationId = 1,
                    LocationName = "Johannesburg",
                    LocationDetails = "Johannesburg"
                },

                new Location()
                {
                    LocationId = 2,
                    LocationName = "Durban",
                    LocationDetails = "Durban"
                },

                new Location()
                {
                    LocationId = 3,
                    LocationName = "Cape Town",
                    LocationDetails = "Cape Town"
                },
            };

            return service;
        }
    }
}

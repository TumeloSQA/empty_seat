using BidFlightWebAPIs.Controllers.BookingTravellers;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BookingTravellers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTestsProject
{
    public class BookingTravellersControllerTest
    {
        private Mock<IBookingTravellersRepository> repoMock;
        private BookingTravellersController controller;

        public BookingTravellersControllerTest()
        {
            repoMock = new Mock<IBookingTravellersRepository>();
            controller = new BookingTravellersController(repoMock.Object);
        }

        [Fact]
        public void TestGetAll()
        {
            //Arrange
            repoMock.Setup(x => x.GetTravellers()).Returns(GetTestService());

            //Act
            var result = controller.GetBookingTraveller();

            //Assert
            var viewResult = Assert.IsType<List<BookingTraveller>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<BookingTraveller>>(result)
;
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void GetById_WhenIdNotFound()
        {
            //Arrange
            repoMock.Setup(x => x.GetTravellers()).Returns(GetTestService());

            // Act
            var result = controller.GetBookingTraveller(0);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetById_WhenIdExists()
        {
            //Arrange
            var mockId = 2;
            var mockTraveller = new BookingTraveller()
            {
                BookingTravellerId = 2,
                UserId = 2,
                Idnumber = "123",
                Title = "Mr",
                FullNames = "john",
                Surname = "doe",
                ContactNumber = "123456"
            };

            repoMock.Setup(repo => repo.GetTraveller(mockId)).Returns(mockTraveller);

            // Act
            var result = controller.GetBookingTraveller(mockId) as OkObjectResult;

            //Assert
            var viewResult = Assert.IsType<BookingTraveller>(result.Value);
            Assert.Equal(mockTraveller,viewResult);
        }

        [Fact]
        public void TestPost_MissingFieldUserId()
        {
            //Arrange
            var newTraveller = new BookingTraveller()
            {
                BookingTravellerId = 1,
                Idnumber = "123",
                Title = "Mr",
                FullNames = "john",
                Surname = "doe",
                ContactNumber = "123456"
            };
            controller.ModelState.AddModelError("UserId","Required");

            //Act
            var result = controller.PostBookingTraveller(newTraveller);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void TestPost_ValidPost()
        {
            //Arrange
            var newTraveller = new BookingTraveller()
            {
                BookingTravellerId = 4,
                UserId = 2,
                Idnumber = "123",
                Title = "Mr",
                FullNames = "john",
                Surname = "doe",
                ContactNumber = "123456"
            };
            repoMock.Setup(repo => repo.AddTraveller(newTraveller));

            //Act
            var response = controller.PostBookingTraveller(newTraveller);

            //Assert
            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public void TestPut_ValidUpdate()
        {
            //Arrange
            var mockId = 1;
            var newTraveller = new BookingTraveller()
            {
                BookingTravellerId = 1,
                UserId = 1,
                Idnumber = "234",
                Title = "Mr",
                FullNames = "john",
                Surname = "doe",
                ContactNumber = "123456"
            };
            repoMock.Setup(repo => repo.UpdateTraveller(newTraveller,mockId));

            //Act
            var result = controller.PutBookingTraveller(mockId, newTraveller) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void TestPut_InvalidId()
        {
            //Arrange
            var mockId = 5;
            var newTraveller = new BookingTraveller()
            {
                BookingTravellerId = 1,
                UserId = 1,
                Idnumber = "234",
                Title = "Mr",
                FullNames = "john",
                Surname = "doe",
                ContactNumber = "123456"
            };
            repoMock.Setup(repo => repo.UpdateTraveller(newTraveller, mockId));

            //Act
            var result = controller.PutBookingTraveller(mockId, newTraveller) as StatusCodeResult;

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        private List<BookingTraveller> GetTestService()
        {
            var service = new List<BookingTraveller>
            {
                new BookingTraveller()
                {
                    BookingTravellerId = 1,
                    UserId = 1,
                    Idnumber = "123",
                    Title = "Mr",
                    FullNames = "john",
                    Surname = "doe",
                    ContactNumber = "123456"
                },
                new BookingTraveller()
                {
                    BookingTravellerId = 2,
                    UserId = 2,
                    Idnumber = "123",
                    Title = "Mr",
                    FullNames = "john",
                    Surname = "doe",
                    ContactNumber = "123456"
                },
                new BookingTraveller()
                {
                    BookingTravellerId = 3,
                    UserId = 3,
                    Idnumber = "123",
                    Title = "Mr",
                    FullNames = "john",
                    Surname = "doe",
                    ContactNumber = "123456"
                }
            };
            return service;
        }
    }
}

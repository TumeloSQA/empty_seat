﻿using BidFlightWebAPIs.Controllers.BillingDetail;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BUnitTestsProject
{
    public class BillingDetailsControllerTest
    {
        private Mock<IBillingDetailsRepository> repoMock;
        private BillingDetailsController controller;

        public BillingDetailsControllerTest()
        {
            repoMock = new Mock<IBillingDetailsRepository>();
            controller = new BillingDetailsController(repoMock.Object);
        }


        [Fact]
        public void TestGetAll()
        {
            //Arrange
            repoMock.Setup(x => x.GetDetails()).Returns(GetTestService());

            //Act
            var result = controller.GetBillingDetails();

            //Assert
            var viewResult = Assert.IsType<List<BillingDetails>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<BillingDetails>>(result)
;
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public void GetById_WhenIdNotFound()
        {
            //Arrange
            var mockId = 1;
            var mockTraveller = new List<BillingDetails>
            {
                new BillingDetails()
                {
                    BillingId = 1,
                    BookingTravellerId = 1,
                    CardName = "Visa",
                    CreditCardNumber = "123",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"

                },
                new BillingDetails()
                {
                    BillingId = 3,
                    BookingTravellerId = 1,
                    CardName = "Mastercard",
                    CreditCardNumber = "789",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"
                }
            };
            repoMock.Setup(x => x.GetBilling(mockId)).Returns(mockTraveller);

            // Act
            var result = controller.GetBillingDetails(0);

            // Assert
            var viewResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GetById_WhenIdExists()
        {
            //Arrange
            var mockId = 1;
            var mockTraveller = new List<BillingDetails>
            {
                new BillingDetails()
                {
                    BillingId = 1,
                    BookingTravellerId = 1,
                    CardName = "Visa",
                    CreditCardNumber = "123",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"

                },
                new BillingDetails()
                {
                    BillingId = 3,
                    BookingTravellerId = 1,
                    CardName = "Mastercard",
                    CreditCardNumber = "789",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"
                }
            };

            repoMock.Setup(repo => repo.GetBilling(mockId)).Returns(mockTraveller);

            // Act
            var result = controller.GetBillingDetails(mockId) as OkObjectResult;

            //Assert
            var viewResult = Assert.IsType<List<BillingDetails>>(result.Value);
            Assert.Equal(mockTraveller, result.Value);
        }

        [Fact]
        public void TestPost_MissingFieldBookingTravellerId()
        {
            //Arrange
            var newTraveller = new BillingDetails()
            {
                BillingId = 5,
                CardName = "American Express",
                CreditCardNumber = "852",
                BillingAddress = "4 street",
                Suburb = "burb4",
                PostalCode = "104",
                CardHolderNames = "Person 4",
                CardHoldersContact = "Contact 4"
            };
            controller.ModelState.AddModelError("UserId", "Required");

            //Act
            var result = controller.PostBillingDetails(newTraveller);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void TestPost_ValidPost()
        {
            //Arrange
            var newTraveller = new BillingDetails()
            {
                BillingId = 5,
                BookingTravellerId = 4,
                CardName = "American Express",
                CreditCardNumber = "852",
                BillingAddress = "4 street",
                Suburb = "burb4",
                PostalCode = "104",
                CardHolderNames = "Person 4",
                CardHoldersContact = "Contact 4"
            };
            repoMock.Setup(repo => repo.AddBillingDetails(newTraveller));

            //Act
            var response = controller.PostBillingDetails(newTraveller);

            //Assert
            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public void TestPut_ValidUpdate()
        {
            //Arrange
            var mockId = 5;
            var newTraveller = new BillingDetails()
            {
                BillingId = 5,
                BookingTravellerId = 4,
                CardName = "Visa",
                CreditCardNumber = "852",
                BillingAddress = "4 street",
                Suburb = "burb4",
                PostalCode = "104",
                CardHolderNames = "Person 4",
                CardHoldersContact = "Contact 4"
            };
            repoMock.Setup(repo => repo.UpdateDetails(newTraveller,mockId));

            //Act
            var result = controller.PutBillingDetails(mockId, newTraveller) as StatusCodeResult;

            //Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void TestPut_InvalidId()
        {
            //Arrange
            var mockId = 7;
            var newTraveller = new BillingDetails()
            {
                BillingId = 5,
                BookingTravellerId = 4,
                CardName = "American Express",
                CreditCardNumber = "852",
                BillingAddress = "4 street",
                Suburb = "burb4",
                PostalCode = "104",
                CardHolderNames = "Person 4",
                CardHoldersContact = "Contact 4"
            };
            repoMock.Setup(repo => repo.UpdateDetails(newTraveller,mockId));

            //Act
            var result = controller.PutBillingDetails(mockId, newTraveller) as StatusCodeResult;

            //Assert
            Assert.IsType<BadRequestResult>(result);
        }

        //[Fact]
        //public void TestDelete_ValidId()
        //{
        //    //Arrange
        //    var newTraveller = new BillingDetails()
        //    {
        //        BillingId = 5,
        //        BookingTravellerId = 4,
        //        CardName = "American Express",
        //        CreditCardNumber = "852",
        //        BillingAddress = "4 street",
        //        Suburb = "burb4",
        //        PostalCode = "104",
        //        CardHolderNames = "Person 4",
        //        CardHoldersContact = "Contact 4"
        //    };
        //    repoMock.Setup(repo => repo.AddBillingDetails(newTraveller));
        //    var controller = new BillingDetailsController(repoMock.Object);
        //    var mockId = 5;

        //    //Act
        //    var add = controller.PostBillingDetails(newTraveller);
        //    var result = controller.DeleteBillingDetails(mockId);

        //    //Assert
        //    Assert.IsType<OkObjectResult>(result);
        //}

        [Fact]
        public void TestDelete_InValidId()
        {
            //Arrange
            var mockId = 10;
            repoMock.Setup(x => x.DeleteBilling(mockId));

            //Act
            var result = controller.DeleteBillingDetails(mockId);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        private List<BillingDetails> GetTestService()
        {
            var service = new List<BillingDetails>
            {
                new BillingDetails()
                {
                    BillingId = 1,
                    BookingTravellerId = 1,
                    CardName = "Visa",
                    CreditCardNumber = "123",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"
                    
                },
                new BillingDetails()
                {
                    BillingId = 2,
                    BookingTravellerId = 2,
                    CardName = "Visa",
                    CreditCardNumber = "456",
                    BillingAddress = "2 street",
                    Suburb = "burb2",
                    PostalCode = "102",
                    CardHolderNames = "Person 2",
                    CardHoldersContact = "Contact 2"
                },
                new BillingDetails()
                {
                    BillingId = 3,
                    BookingTravellerId = 1,
                    CardName = "Mastercard",
                    CreditCardNumber = "789",
                    BillingAddress = "1 street",
                    Suburb = "burb1",
                    PostalCode = "101",
                    CardHolderNames = "Person 1",
                    CardHoldersContact = "Contact 1"
                },
                new BillingDetails()
                {
                    BillingId = 4,
                    BookingTravellerId = 3,
                    CardName = "Mastercard",
                    CreditCardNumber = "147",
                    BillingAddress = "3 street",
                    Suburb = "burb3",
                    PostalCode = "103",
                    CardHolderNames = "Person 3",
                    CardHoldersContact = "Contact 3"
                }
            };
            return service;
        }
    }
}

﻿using ClientModule.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientModule.ServiceInterfaces.CustomerAuthentication
{
    public interface ICustomerAuthRepository
    {
        IEnumerable<Users> GetBookingTravellers();
        Users GetByUsername(string username);
        void RegisterBookingUser([FromBody]BookingUserViewModel bookingUserViewModel);

        void Update([FromBody]BookingUserViewModel bookingUserViewModel, int userID, string password = null);
        //void Delete(int id);
        void Save();
    }
}

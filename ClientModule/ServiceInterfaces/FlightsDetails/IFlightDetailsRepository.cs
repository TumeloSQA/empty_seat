﻿using System;
using System.Collections.Generic;
using System.Text;
using ClientModule.Models;

namespace ClientModule.ServiceInterfaces.FlightsDetails
{
    public interface IFlightDetailsRepository
    {
        IEnumerable<FlightDetails> GetAllFlights();
        IEnumerable<FlightDetails> GetAirlineFlights(int id);
        IEnumerable<FlightDetails> GetSearchFlights(FlightSearchModel flight);
        void AddFlight(FlightDetails flightDetails);
        void UpdateFlight(FlightDetails flightDetails, int id);
        bool DeleteFlight(int id); 
    }
}

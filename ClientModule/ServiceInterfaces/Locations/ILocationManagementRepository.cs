﻿using ClientModule.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ClientModule.ServiceInterfaces.Locations
{
    public interface ILocationManagementRepository
    {
        IEnumerable<Location> GetLocations();
        void AddLocation(Location location);
        Location GetLocation(string locationName);
        void Save();
    }
}

﻿using ClientModule.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.ServiceInterfaces.BidFlight
{
    public interface IReservationRepository
    {
        IEnumerable<Reservation> GetReservations();
        IEnumerable<Reservation> GetReservation(int id);
        void NewReservation(ReservationPaymentViewModel reservationPaymentViewModel);
        void UpdateResevation(Reservation reservation, int id);
        bool DeleteReservation(int id);
    }
}

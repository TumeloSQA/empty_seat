﻿using ClientModule.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.ServiceInterfaces.BidFlight
{
    public interface IBillingDetailsRepository
    {
        IEnumerable<BillingDetails> GetDetails();
        IEnumerable<BillingDetails> GetBilling(int id);
        void AddBillingDetails(BillingDetails billingDetails);
        void UpdateDetails(BillingDetails billingDetails, int id);
        bool DeleteBilling(int id);
    }
}

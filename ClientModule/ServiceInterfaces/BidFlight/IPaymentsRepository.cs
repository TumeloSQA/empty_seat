﻿using ClientModule.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.ServiceInterfaces.BidFlight
{
    public interface IPaymentsRepository
    {
        IEnumerable<Payments> GetPayments();
        IEnumerable<Payments> GetPayment(int id);
        void AddBillingDetails(Payments payments);
        void UpdateDetails(Payments payments, int id);
        bool DeletePayment(int id);
    }
}

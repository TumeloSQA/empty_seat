﻿using ClientModule.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.ServiceInterfaces.BookingTravellers
{
    public interface IBookingTravellersRepository
    {
        IEnumerable<BookingTraveller> GetTravellers();
        BookingTraveller GetTraveller(int id);
        void AddTraveller(BookingTraveller traveller);
        void UpdateTraveller(BookingTraveller traveller, int id);
    }
}

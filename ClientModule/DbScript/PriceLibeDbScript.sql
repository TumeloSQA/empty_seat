USE [master]
GO
/****** Object:  Database [PriceLineDb]    Script Date: 11/26/2018 2:25:08 PM ******/
CREATE DATABASE [PriceLineDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PriceLineDb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\PriceLineDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PriceLineDb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\PriceLineDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PriceLineDb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PriceLineDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PriceLineDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PriceLineDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PriceLineDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PriceLineDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PriceLineDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [PriceLineDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PriceLineDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PriceLineDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PriceLineDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PriceLineDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PriceLineDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PriceLineDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PriceLineDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PriceLineDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PriceLineDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PriceLineDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PriceLineDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PriceLineDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PriceLineDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PriceLineDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PriceLineDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PriceLineDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PriceLineDb] SET RECOVERY FULL 
GO
ALTER DATABASE [PriceLineDb] SET  MULTI_USER 
GO
ALTER DATABASE [PriceLineDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PriceLineDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PriceLineDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PriceLineDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PriceLineDb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PriceLineDb', N'ON'
GO
ALTER DATABASE [PriceLineDb] SET QUERY_STORE = OFF
GO
USE [PriceLineDb]
GO
/****** Object:  UserDefinedFunction [dbo].[ReferenceNumber_fn]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ReferenceNumber_fn] (@ReservationID int)
 
returns char(13) 
as 
begin 
DECLARE @date datetime = getdate();
return 'REF' + format(@date, 'yyMMdd') + right('000' + convert(varchar(10), @ReservationID), 3) 
end
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Username] [varchar](20) NOT NULL,
	[Password] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Airline]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airline](
	[AgentID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[FligthName] [varchar](20) NOT NULL,
	[FlightDetails] [varchar](20) NOT NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_Agent] PRIMARY KEY CLUSTERED 
(
	[AgentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillingDetails]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingDetails](
	[BillingID] [int] IDENTITY(1,1) NOT NULL,
	[BookingTravellerID] [int] NOT NULL,
	[CardName] [varchar](10) NOT NULL,
	[CreditCardNumber] [varchar](20) NOT NULL,
	[BillingAddress] [varchar](50) NOT NULL,
	[Suburb] [varchar](50) NOT NULL,
	[PostalCode] [varchar](10) NOT NULL,
	[CardHolderNames] [varchar](20) NOT NULL,
	[CardHoldersContact] [varchar](20) NOT NULL,
 CONSTRAINT [PK_BillingDetails] PRIMARY KEY CLUSTERED 
(
	[BillingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookingTraveller]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookingTraveller](
	[BookingTravellerID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[IDNumber] [varchar](20) NOT NULL,
	[Title] [varchar](10) NOT NULL,
	[FullNames] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[ContactNumber] [varchar](20) NOT NULL,
 CONSTRAINT [PK_BookingTraveller] PRIMARY KEY CLUSTERED 
(
	[BookingTravellerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [varchar](50) NOT NULL,
	[LocationDetails] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payments]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[PaymentID] [int] IDENTITY(1,1) NOT NULL,
	[BillingID] [int] NOT NULL,
	[ReservationID] [int] NOT NULL,
	[Subtotal] [decimal](18, 0) NOT NULL,
	[Fees] [decimal](18, 0) NOT NULL,
	[Tax] [decimal](18, 0) NOT NULL,
	[TotalAmount] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reservation]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reservation](
	[ReservationID] [int] IDENTITY(1,1) NOT NULL,
	[BookingTravellerID] [int] NOT NULL,
	[NumberOfTravellers] [varchar](10) NOT NULL,
	[Status] [varchar](20) NULL,
	[Origin] [varchar](50) NOT NULL,
	[Destination] [varchar](50) NOT NULL,
	[DepartingDate] [varchar](20) NOT NULL,
	[ArrivalDate] [varchar](20) NOT NULL,
	[ReferenceNumber]  AS ([dbo].[ReferenceNumber_fn]([ReservationID])),
 CONSTRAINT [PK_Reservation] PRIMARY KEY CLUSTERED 
(
	[ReservationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/26/2018 2:25:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[RoleID] [int] NOT NULL,
	[Password] [varchar](100) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD  CONSTRAINT [FK_Users_Admin] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Admin] CHECK CONSTRAINT [FK_Users_Admin]
GO
ALTER TABLE [dbo].[Airline]  WITH CHECK ADD  CONSTRAINT [FK_Users_Traveller] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Airline] CHECK CONSTRAINT [FK_Users_Traveller]
GO
ALTER TABLE [dbo].[BillingDetails]  WITH CHECK ADD  CONSTRAINT [FK_BookingTraveller_BillingDetails] FOREIGN KEY([BookingTravellerID])
REFERENCES [dbo].[BookingTraveller] ([BookingTravellerID])
GO
ALTER TABLE [dbo].[BillingDetails] CHECK CONSTRAINT [FK_BookingTraveller_BillingDetails]
GO
ALTER TABLE [dbo].[BookingTraveller]  WITH CHECK ADD  CONSTRAINT [FK_Users_BookingTraveller] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[BookingTraveller] CHECK CONSTRAINT [FK_Users_BookingTraveller]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_BillingDetails] FOREIGN KEY([BillingID])
REFERENCES [dbo].[BillingDetails] ([BillingID])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_BillingDetails]
GO
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Reservations] FOREIGN KEY([ReservationID])
REFERENCES [dbo].[Reservation] ([ReservationID])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_Reservations]
GO
ALTER TABLE [dbo].[Reservation]  WITH CHECK ADD  CONSTRAINT [FK_Reservation_BookingTraveller] FOREIGN KEY([BookingTravellerID])
REFERENCES [dbo].[BookingTraveller] ([BookingTravellerID])
GO
ALTER TABLE [dbo].[Reservation] CHECK CONSTRAINT [FK_Reservation_BookingTraveller]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([RoleID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]
GO
USE [master]
GO
ALTER DATABASE [PriceLineDb] SET  READ_WRITE 
GO

﻿using ClientModule.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ClientModule.ModelMappings
{
    public class RoleType : IEntityTypeConfiguration<Roles>
    {
        public void Configure(EntityTypeBuilder<Roles> builder)
        {
            builder.HasKey(x => x.RoleId);
            builder.HasMany(e => e.Users).WithOne(o => o.Role).HasForeignKey(e => e.RoleId);
            builder.HasQueryFilter(o => o.RoleId == 1);
        }
    }
}

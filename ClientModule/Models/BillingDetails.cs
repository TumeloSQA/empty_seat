﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class BillingDetails
    {
        public BillingDetails()
        {
            Payments = new HashSet<Payments>();
        }

        public int BillingId { get; set; }
        public int BookingTravellerId { get; set; }
        public string CardName { get; set; }
        public string CreditCardNumber { get; set; }
        public string BillingAddress { get; set; }
        public string Suburb { get; set; }
        public string PostalCode { get; set; }
        public string CardHolderNames { get; set; }
        public string CardHoldersContact { get; set; }

        public BookingTraveller BookingTraveller { get; set; }
        public ICollection<Payments> Payments { get; set; }
    }
}

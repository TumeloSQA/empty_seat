﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Location
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationDetails { get; set; }
    }
}

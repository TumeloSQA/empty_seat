﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Admin
    {
        public int AdminId { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Users User { get; set; }
    }
}

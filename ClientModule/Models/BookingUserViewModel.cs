﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.Models
{
    public class BookingUserViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Idnumber { get; set; }
        public string Title { get; set; }
        public string FullNames { get; set; }
        public string Surname { get; set; }
        public string ContactNumber { get; set; }

    }
}

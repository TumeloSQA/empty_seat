﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.Models
{
    public class ReservationPaymentViewModel
    {

        public int BookingTravellerId { get; set; }
        public int AgentId { get; set; }
        public int NumberOfTravellers { get; set; }
        public string Status { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string DepartingDate { get; set; }
        public string DepartingTime { get; set; }

        public int BillingId { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Fees { get; set; } = 150.00M;
        public decimal Tax { get; set; } = 0.15M;
        public decimal TotalAmount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.Models
{
    public class FlightSearchModel
    {
        public string Date { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
    }
}

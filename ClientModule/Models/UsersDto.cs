﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientModule.Models
{
    public class UsersDto
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

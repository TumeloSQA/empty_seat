﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Airline
    {
        public Airline()
        {
            FlightDetails = new HashSet<FlightDetails>();
            Reservation = new HashSet<Reservation>();
        }

        public int AgentId { get; set; }
        public int UserId { get; set; }
        public string Status { get; set; }
        public string AirlineName { get; set; }
        public string AirlineDetails { get; set; }

        public Users User { get; set; }
        public ICollection<FlightDetails> FlightDetails { get; set; }
        public ICollection<Reservation> Reservation { get; set; }
    }
}

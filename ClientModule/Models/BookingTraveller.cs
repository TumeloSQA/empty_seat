﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class BookingTraveller
    {
        public BookingTraveller()
        {
            BillingDetails = new HashSet<BillingDetails>();
            Reservation = new HashSet<Reservation>();
        }

        public int BookingTravellerId { get; set; }
        public int UserId { get; set; }
        public string Idnumber { get; set; }
        public string Title { get; set; }
        public string FullNames { get; set; }
        public string Surname { get; set; }
        public string ContactNumber { get; set; }

        public Users User { get; set; }
        public ICollection<BillingDetails> BillingDetails { get; set; }
        public ICollection<Reservation> Reservation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Reservation
    {
        public Reservation()
        {
            Payments = new HashSet<Payments>();
        }

        public int ReservationId { get; set; }
        public int BookingTravellerId { get; set; }
        public int AgentId { get; set; }
        public int NumberOfTravellers { get; set; }
        public string Status { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string DepartingDate { get; set; }
        public string DepartingTime { get; set; }
        public string ReferenceNumber { get; set; }

        public Airline Agent { get; set; }
        public BookingTraveller BookingTraveller { get; set; }
        public ICollection<Payments> Payments { get; set; }
    }
}

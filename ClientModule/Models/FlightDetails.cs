﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class FlightDetails
    {
        public int FlightDetailsId { get; set; }
        public int AgentId { get; set; }
        public string Date { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public int Seats { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public decimal Price { get; set; }

        public Airline Agent { get; set; }
    }
}

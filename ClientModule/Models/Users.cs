﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Users
    {
        public Users()
        {
            Admin = new HashSet<Admin>();
            Airline = new HashSet<Airline>();
            BookingTraveller = new HashSet<BookingTraveller>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }

        public Roles Role { get; set; }
        public ICollection<Admin> Admin { get; set; }
        public ICollection<Airline> Airline { get; set; }
        public ICollection<BookingTraveller> BookingTraveller { get; set; }
    }
}

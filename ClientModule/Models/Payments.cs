﻿using System;
using System.Collections.Generic;

namespace ClientModule.Models
{
    public partial class Payments
    {
        public int PaymentId { get; set; }
        public int BillingId { get; set; }
        public int ReservationId { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Fees { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalAmount { get; set; }

        public BillingDetails Billing { get; set; }
        public Reservation Reservation { get; set; }
    }
}

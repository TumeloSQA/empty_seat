﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ClientModule.Migrations
{
    public partial class PriceLine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LocationName = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    LocationDetails = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Username = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                    table.ForeignKey(
                        name: "FK_Users_Roles",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    AdminID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    Username = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Password = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.AdminID);
                    table.ForeignKey(
                        name: "FK_Users_Admin",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Airline",
                columns: table => new
                {
                    AgentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    FligthName = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    FlightDetails = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Status = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airline", x => x.AgentID);
                    table.ForeignKey(
                        name: "FK_Users_Traveller",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingTraveller",
                columns: table => new
                {
                    BookingTravellerID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<int>(nullable: false),
                    IDNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Title = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    FullNames = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Surname = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ContactNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingTraveller", x => x.BookingTravellerID);
                    table.ForeignKey(
                        name: "FK_Users_BookingTraveller",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BillingDetails",
                columns: table => new
                {
                    BillingID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BookingTravellerID = table.Column<int>(nullable: false),
                    CardName = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    CreditCardNumber = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    BillingAddress = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Suburb = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PostalCode = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    CardHolderNames = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    CardHoldersContact = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillingDetails", x => x.BillingID);
                    table.ForeignKey(
                        name: "FK_BookingTraveller_BillingDetails",
                        column: x => x.BookingTravellerID,
                        principalTable: "BookingTraveller",
                        principalColumn: "BookingTravellerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reservation",
                columns: table => new
                {
                    ReservationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BookingTravellerID = table.Column<int>(nullable: false),
                    ReferenceNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    NumberOfTravellers = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    Status = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    Origin = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Destination = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    DepartingDate = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    ArrivalDate = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservation", x => x.ReservationID);
                    table.ForeignKey(
                        name: "FK_Reservation_BookingTraveller",
                        column: x => x.BookingTravellerID,
                        principalTable: "BookingTraveller",
                        principalColumn: "BookingTravellerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    PaymentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BillingID = table.Column<int>(nullable: false),
                    Subtotal = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Fees = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Tax = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TotalAmount = table.Column<string>(unicode: false, maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.PaymentID);
                    table.ForeignKey(
                        name: "FK_Payments_BillingDetails",
                        column: x => x.BillingID,
                        principalTable: "BillingDetails",
                        principalColumn: "BillingID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Admin_UserID",
                table: "Admin",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Airline_UserID",
                table: "Airline",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_BillingDetails_BookingTravellerID",
                table: "BillingDetails",
                column: "BookingTravellerID");

            migrationBuilder.CreateIndex(
                name: "IX_BookingTraveller_UserID",
                table: "BookingTraveller",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_BillingID",
                table: "Payments",
                column: "BillingID");

            migrationBuilder.CreateIndex(
                name: "IX_Reservation_BookingTravellerID",
                table: "Reservation",
                column: "BookingTravellerID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleID",
                table: "Users",
                column: "RoleID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "Airline");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Reservation");

            migrationBuilder.DropTable(
                name: "BillingDetails");

            migrationBuilder.DropTable(
                name: "BookingTraveller");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}

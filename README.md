**Prerequisite**

Following tools/softwares must be installed on your machine :

| Name        | Version           |
| ------      | -------           |
| ASP.NET Core| 2.1               |
| GIT         |                   |
| Maven       | 3.x               |
| NodeJS(npm) | 8.12.x            |
| Angular     | 5                 |
| IDE         | Visual Studio     |

Note :

Suggested IDE Visual Studio, Visual Studio Code, SQL Server Management Studio

**Project Structure**

* ``client-module  :`` Includes the service interface and service level models(dto) only.
* ``service-module :`` Includes the service implementation integrated with other systems.

Module Dependency  : ``client-module`` <-------- ``service-module





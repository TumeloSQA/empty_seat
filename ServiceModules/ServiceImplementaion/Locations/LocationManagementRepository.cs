﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.Locations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceModules.ServiceImplementaion.Locations
{
    public class LocationManagementRepository : ILocationManagementRepository
    {
        private readonly PriceLineDbContext _context;

        public LocationManagementRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Location> GetLocations()
        {
            return _context.Location.ToList();
        }

        public void AddLocation(Location location)
        {
            try
            {
                if (LocationExists(location.LocationName))
                {
                    throw new Exception();
                }
                _context.Location.Add(location);
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Location GetLocation(string locationName)
        {
            return _context.Location.FirstOrDefault();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool LocationExists(string locationName)
        {
            return _context.Location.Any(e => e.LocationName == locationName);
        }
    }
}

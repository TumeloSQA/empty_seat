﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceModules.ServiceImplementaion.BidFlight
{
    public class ReservationRepository : IReservationRepository
    {
        private readonly PriceLineDbContext _context;

        public ReservationRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public bool DeleteReservation(int id)
        {
            var reservation = _context.Reservation.Find(id);
            if (reservation == null)
            {
                return false;
            }
             _context.Reservation.Remove(reservation);

             _context.SaveChanges();

            return true;
        }

        public IEnumerable<Reservation> GetReservation(int id)
        {
            return _context.Reservation.Where(r => r.BookingTravellerId == id).ToList();
        }

        public IEnumerable<Reservation> GetReservations()
        {
            return _context.Reservation.ToList();
        }

        public void NewReservation(ReservationPaymentViewModel reservationPaymentViewModel)
        {
            Reservation reservation = new Reservation()
            {
                Origin = reservationPaymentViewModel.Origin,
                Destination = reservationPaymentViewModel.Destination,
                DepartingDate = reservationPaymentViewModel.DepartingDate,
                DepartingTime = reservationPaymentViewModel.DepartingTime,
                NumberOfTravellers = reservationPaymentViewModel.NumberOfTravellers,
                BookingTravellerId = reservationPaymentViewModel.BookingTravellerId,
                AgentId = reservationPaymentViewModel.AgentId,
                Status = "Awaiting Approval"
            };

            _context.Reservation.Add(reservation);

            Payments payments = new Payments()
            {
                BillingId = reservationPaymentViewModel.BillingId,
                Subtotal = reservationPaymentViewModel.Subtotal,
                Fees = reservationPaymentViewModel.Fees,
                Tax = reservationPaymentViewModel.Tax * reservationPaymentViewModel.Subtotal, 
                TotalAmount = reservationPaymentViewModel.Subtotal + reservationPaymentViewModel.Fees + (reservationPaymentViewModel.Tax * reservationPaymentViewModel.Subtotal)
            };

            payments.ReservationId = reservation.ReservationId;

            _context.Payments.Add(payments);

            _context.SaveChanges();
        }

        public void UpdateResevation(Reservation reservation, int id)
        {
            try
            {
                if (!ReservationExists(id))
                {
                    throw new KeyNotFoundException();
                }
                _context.Entry(reservation).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }

        private bool ReservationExists(int id)
        {
            return _context.Reservation.Any(e => e.ReservationId == id);
        }
    }
}

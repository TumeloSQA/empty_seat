﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceModules.ServiceImplementaion.BidFlight
{
    public class BillingDetailsRepository : IBillingDetailsRepository
    {
        private readonly PriceLineDbContext _context;

        public BillingDetailsRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public void AddBillingDetails(BillingDetails billingDetails)
        {
            _context.BillingDetails.Add(billingDetails);
            _context.SaveChanges();
        }

        public bool DeleteBilling(int id)
        {
            var billing = _context.BillingDetails.Find(id);
            if (billing == null)
            {
                return false;
            }
            _context.BillingDetails.Remove(billing);
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<BillingDetails> GetBilling(int id)
        {
            return _context.BillingDetails.Where(b => b.BookingTravellerId == id).ToList();
        }

        public IEnumerable<BillingDetails> GetDetails()
        {
            return _context.BillingDetails.ToList();
        }

        public void UpdateDetails(BillingDetails billingDetails, int id)
        {
            try
            {
                if (!BillingDetailsExists(id))
                {
                    throw new KeyNotFoundException();
                }
                _context.Entry(billingDetails).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }

        private bool BillingDetailsExists(int id)
        {
            return _context.BillingDetails.Any(e => e.BillingId == id);
        }
    }
}

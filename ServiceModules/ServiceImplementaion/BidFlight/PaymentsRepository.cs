﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceModules.ServiceImplementaion.BidFlight
{
    public class PaymentsRepository : IPaymentsRepository
    {
        private readonly PriceLineDbContext _context;

        public PaymentsRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public void AddBillingDetails(Payments payments)
        {
            Payments payment = new Payments()
            {

            };

            _context.Payments.Add(payments);

            _context.SaveChanges();
        }

        public bool DeletePayment(int id)
        {
            var payment = _context.Reservation.Find(id);
            if (payment == null)
            {
                return false;
            }
            _context.Reservation.Remove(payment);
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<Payments> GetPayment(int id)
        {
            return _context.Payments.Where(p => p.BillingId == id).ToList();
        }

        public IEnumerable<Payments> GetPayments()
        {
            return _context.Payments.ToList();
        }

        public void UpdateDetails(Payments payments, int id)
        {
            try
            {
                if(!PaymentsExists(id))
                {
                    throw new KeyNotFoundException();
                }
                _context.Entry(payments).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }

        private bool PaymentsExists(int id)
        {
            return _context.Payments.Any(e => e.PaymentId == id);
        }
    }
}

﻿using ClientModule.Helpers;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.CustomerAuthentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ServiceModules.ServiceImplementaion.CustomerAuthentication
{
    public class CustomerAuthRepository: ICustomerAuthRepository
    {
        private readonly PriceLineDbContext _context;

        public CustomerAuthRepository(PriceLineDbContext context)
        {
            _context = context;
        }
        
        public IEnumerable<Users> GetBookingTravellers()
        {
            IList<Users> users = null;
            using (var context = _context)
            {
                users = _context.Users
                    // .Include(user => user.Role).Select(x => x)
                    .Include(traveller => traveller.BookingTraveller)
                    .Where(role => role.Role.RoleId == 1)
                    .ToList();
                return users;
            }
        }
        
        public void RegisterBookingUser(BookingUserViewModel bookingUserViewModel)
        {
            // validation
            if (string.IsNullOrWhiteSpace(bookingUserViewModel.Password))
                throw new AppException("Password is required");

            //Check if User exist
            if (_context.Users.Any(x => x.Username == bookingUserViewModel.Username))
                throw new AppException("Username \"" + bookingUserViewModel.Username + "\" is already taken");
            
            Users users = new Users
            {
                Username = bookingUserViewModel.Username,
                Password = bookingUserViewModel.Password,
                RoleId = 1
            };

            _context.Users.Add(users);

            BookingTraveller bookingTraveller = new BookingTraveller
            {
                Title = bookingUserViewModel.Title,
                FullNames = bookingUserViewModel.FullNames,
                Surname = bookingUserViewModel.Surname,
                Idnumber = bookingUserViewModel.Idnumber,
                ContactNumber = bookingUserViewModel.ContactNumber
            };

            bookingTraveller.UserId = users.UserId;
            _context.BookingTraveller.Add(bookingTraveller);
            _context.SaveChanges();
        }

        public Users GetByUsername(string username)
        {
            return _context.Users.Find(username);
        }

        public void Update(BookingUserViewModel bookingUserViewModel, int userID, string password = null)
        {
            
            var user = _context.Users.Find(userID);
            
            if (user == null)
            {
                throw new AppException("User not found");
            }

            if (bookingUserViewModel.Username != user.Username)
            {
                // username has changed so check if the new username is already taken
                if (_context.Users.Any(x => x.Username == bookingUserViewModel.Username))
                    throw new AppException("Username " + bookingUserViewModel.Username + " is already taken");
            }

            // update user properties
            user.Username = bookingUserViewModel.Username;
            
            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(password))
            {
                user.Password = password;
            }

            _context.Users.Update(user);

            //Update Booking Traveller Details
            var id = _context.BookingTraveller.SingleOrDefault(x => x.UserId == userID);

            var traveller = _context.BookingTraveller.Find(id.BookingTravellerId);

            BookingTraveller bookingTraveller = _context.BookingTraveller.Find(traveller.BookingTravellerId);

            if (!string.IsNullOrWhiteSpace(bookingUserViewModel.FullNames))
            {
                bookingTraveller.FullNames = bookingUserViewModel.FullNames;
            }

            if (!string.IsNullOrWhiteSpace(bookingUserViewModel.Surname))
            {
                bookingTraveller.Surname = bookingUserViewModel.Surname;
            }

            if (!string.IsNullOrWhiteSpace(bookingUserViewModel.Idnumber))
            {
                bookingTraveller.Idnumber = bookingUserViewModel.Idnumber;
            }

            if (!string.IsNullOrWhiteSpace(bookingUserViewModel.Title))
            {
                bookingTraveller.Title = bookingUserViewModel.Title;
            }

            if (!string.IsNullOrWhiteSpace(bookingUserViewModel.ContactNumber))
            {
                bookingTraveller.ContactNumber = bookingUserViewModel.ContactNumber;
            }

            _context.BookingTraveller.Update(bookingTraveller);
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            if (storedHash.Length != 64)
            {
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            }

            if (storedSalt.Length != 128)
            {
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");
            }

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}

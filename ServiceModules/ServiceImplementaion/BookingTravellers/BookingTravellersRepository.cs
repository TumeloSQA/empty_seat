﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.BookingTravellers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceModules.ServiceImplementaion.BookingTravellers
{
    public class BookingTravellersRepository : IBookingTravellersRepository
    {
        private readonly PriceLineDbContext _context;

        public BookingTravellersRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public void AddTraveller(BookingTraveller traveller)
        {
            _context.BookingTraveller.Add(traveller);
            _context.SaveChanges();
        }

        public BookingTraveller GetTraveller(int id)
        {
            return _context.BookingTraveller.Where(b => b.UserId == id).FirstOrDefault();
        }

        public IEnumerable<BookingTraveller> GetTravellers()
        {
            return _context.BookingTraveller.ToList();
        }

        public void UpdateTraveller(BookingTraveller traveller, int id)
        {
            try
            {
                if(!BookingTravellerExists(id))
                {
                    throw new KeyNotFoundException();
                }
                _context.Entry(traveller).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }

        private bool BookingTravellerExists(int id)
        {
            return _context.BookingTraveller.Any(e => e.BookingTravellerId == id);
        }
    }
}

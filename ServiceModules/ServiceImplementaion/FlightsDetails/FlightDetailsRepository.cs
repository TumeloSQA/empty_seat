﻿using ClientModule.Models;
using ClientModule.ServiceInterfaces.FlightsDetails;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceModules.ServiceImplementaion.FlightsDetails
{
    public class FlightDetailsRepository : IFlightDetailsRepository
    {
        private readonly PriceLineDbContext _context;

        public FlightDetailsRepository(PriceLineDbContext context)
        {
            _context = context;
        }

        public void AddFlight(FlightDetails flightDetails)
        {
            _context.FlightDetails.Add(flightDetails);
            _context.SaveChanges();
        }

        public bool DeleteFlight(int id)
        {
            var flight = _context.FlightDetails.Find(id);
            if (flight == null)
            {
                return false;
            }
            _context.FlightDetails.Remove(flight);
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<FlightDetails> GetAirlineFlights(int id)
        {
            return _context.FlightDetails.Where(e => e.AgentId == id);
        }

        public IEnumerable<FlightDetails> GetAllFlights()
        {
            return _context.FlightDetails.ToList();
        }

        public IEnumerable<FlightDetails> GetSearchFlights(FlightSearchModel flight)
        {
            return _context.FlightDetails.Where(e => e.Date == flight.Date && e.Origin == flight.Origin && e.Destination == flight.Destination).ToList();
        }

        public void UpdateFlight(FlightDetails flightDetails, int id)
        {
            try
            {
                if (!FlightDetailsExists(id))
                {
                    throw new KeyNotFoundException();
                }
                _context.Entry(flightDetails).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            catch (KeyNotFoundException)
            {
                throw;
            }
        }

        private bool FlightDetailsExists(int id)
        {
            return _context.FlightDetails.Any(e => e.FlightDetailsId == id);
        }
    }
}

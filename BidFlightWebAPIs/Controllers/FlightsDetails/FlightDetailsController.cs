﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.FlightsDetails;

namespace BidFlightWebAPIs.Controllers.FlightsDetails
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightDetailsController : ControllerBase
    {
        private readonly IFlightDetailsRepository _repository;

        public FlightDetailsController(IFlightDetailsRepository repository)
        {
            _repository = repository;
        }

        // GET: api/FlightDetails
        [HttpGet]
        public IEnumerable<FlightDetails> GetFlightDetails()
        {
            return _repository.GetAllFlights();
        }

        // GET: api/FlightDetails/5
        [HttpGet("{id}")]
        public IActionResult GetFlightDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var flightDetails = _repository.GetAirlineFlights(id);

            if (flightDetails.Count() == 0)
            {
                return NotFound();
            }

            return Ok(flightDetails);
        }

        // GET: api/FlightDetails/Search
        [HttpGet]
        [Route("Search")]
        public IActionResult GetFlightDetails([FromBody] FlightSearchModel flight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var flightDetails = _repository.GetSearchFlights(flight);

            if (flightDetails.Count() == 0)
            {
                return NotFound();
            }

            return Ok(flightDetails);
        }

        // PUT: api/FlightDetails/5
        [HttpPut("{id}")]
        public IActionResult PutFlightDetails([FromRoute] int id, [FromBody] FlightDetails flightDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != flightDetails.FlightDetailsId)
            {
                return BadRequest();
            }            

            try
            {
                _repository.UpdateFlight(flightDetails, id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/FlightDetails
        [HttpPost]
        public IActionResult PostFlightDetails([FromBody] FlightDetails flightDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.AddFlight(flightDetails);

            return CreatedAtAction("GetFlightDetails", new { id = flightDetails.FlightDetailsId }, flightDetails);
        }

        // DELETE: api/FlightDetails/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFlightDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_repository.DeleteFlight(id))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
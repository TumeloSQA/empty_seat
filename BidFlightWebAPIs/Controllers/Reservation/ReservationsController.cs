﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Cors;

namespace BidFlightWebAPIs.Controllers.Reservations
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly IReservationRepository _reservationRepository;

        public ReservationsController(IReservationRepository reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        // GET: api/Reservations
        [HttpGet]
        public IEnumerable<Reservation> GetReservation()
        {
            return _reservationRepository.GetReservations();
        }

        // GET: api/Reservations/5
        [HttpGet("{id}")]
        public IActionResult GetReservation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reservation = _reservationRepository.GetReservation(id);

            if (reservation.Count() == 0)
            {
                return NotFound();
            }

            return Ok(reservation);
        }

        // PUT: api/Reservations/5
        [HttpPut("{id}")]
        public IActionResult PutReservation([FromRoute] int id, [FromBody] Reservation reservation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservation.ReservationId)
            {
                return BadRequest();
            } 

            try
            {
                _reservationRepository.UpdateResevation(reservation, id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Reservations
        [HttpPost]
        public IActionResult PostReservation([FromBody] ReservationPaymentViewModel reservationPaymentViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _reservationRepository.NewReservation(reservationPaymentViewModel);

            return CreatedAtAction("GetReservation", new { id = reservationPaymentViewModel.Status }, reservationPaymentViewModel);
        }

        // DELETE: api/Reservations/5
        [HttpDelete("{id}")]
        public IActionResult DeleteReservation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_reservationRepository.DeleteReservation(id))
            {
                return NotFound();
            }
            
            return Ok();
        }
    }
}
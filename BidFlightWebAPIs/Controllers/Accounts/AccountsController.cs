﻿using AutoMapper;
using BidFlightWebAPIs;
using ClientModule.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

[Route("api/[AccountsController]")]
public class AccountsController : Controller
{
    private readonly SignInManager<User> _signInManager;
    
    private readonly PriceLineDbContext _priceLineDbContext;

    public AccountsController(SignInManager<User> signInManager, PriceLineDbContext priceLineDbContext)
	{
        _signInManager = signInManager;
        _priceLineDbContext = priceLineDbContext;
       

    }
    [HttpGet]
    public IActionResult Login()
    {
        if (this.User.Identity.IsAuthenticated)
        {
            // Which redirect if user is logged in
            return RedirectToAction("Index", "Home");
        }
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> Logout()
    {
        await _signInManager.SignOutAsync();
        // Which redirect after logging out
        return RedirectToAction("Index", "Home");
    }

    
    [HttpPost]
    public async Task<IActionResult> Login(User usersModel)
    {
        if (ModelState.IsValid)
        {

           var result = await _signInManager.PasswordSignInAsync(
               usersModel.Username,
               usersModel.Password,
               true,
               false
                );
            if (result.Succeeded)
            {
                if (Request.Query.Keys.Contains("ReturnUrl"))
                {
                    return Redirect(Request.Query["ReturnUrl"].First());
                }
                else
                {
                    // Which redirect after logging in
                    RedirectToAction("Index", "Home");
                }
            }
        }
        ModelState.AddModelError("", "Failed to login");
        return View();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.Locations;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceModules.ServiceImplementaion.Locations;

namespace BidFlightWebAPIs.Controllers.Locations
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class LocationManagementController : ControllerBase
    {
        private readonly ILocationManagementRepository _repository;

        public LocationManagementController(ILocationManagementRepository repository)
        {
            _repository = repository;
        }

        // GET: api/LocationManagement
        [HttpGet]
        public IEnumerable<Location> GetLocations()
        {
            return _repository.GetLocations().ToList();
        }


        // POST: api/LocationManagement
        [HttpPost]
        public IActionResult PostLocation([FromBody] Location location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            try
            {
                _repository.AddLocation(location);
            }
            catch (DbUpdateException)
            {
                throw;
            }
            catch (Exception)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            return CreatedAtAction("GetLocations", new { id = location.LocationId }, location);
        }

        [HttpGet]
        [Route("getLocationByName")]
        public Location GetLocation(string locationName)
        {
            return _repository.GetLocation(locationName);
        }
        
    }
}
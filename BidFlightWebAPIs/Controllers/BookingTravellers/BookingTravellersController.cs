﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BookingTravellers;
using Microsoft.AspNetCore.Cors;

namespace BidFlightWebAPIs.Controllers.BookingTravellers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class BookingTravellersController : ControllerBase
    {
        private readonly IBookingTravellersRepository _repository;

        public BookingTravellersController(IBookingTravellersRepository repository)
        {
            _repository = repository;
        }

        // GET: api/BookingTravellers
        [HttpGet]
        public IEnumerable<BookingTraveller> GetBookingTraveller()
        {
            return _repository.GetTravellers();
        }

        // GET: api/BookingTravellers/5
        [HttpGet("{id}")]
        public IActionResult GetBookingTraveller([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookingTraveller = _repository.GetTraveller(id);

            if (bookingTraveller == null)
            {
                return NotFound();
            }

            return Ok(bookingTraveller);
        }

        // PUT: api/BookingTravellers/5
        [HttpPut("{id}")]
        public IActionResult PutBookingTraveller([FromRoute] int id, [FromBody] BookingTraveller bookingTraveller)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bookingTraveller.BookingTravellerId)
            {
                return BadRequest();
            }            

            try
            {
                _repository.UpdateTraveller(bookingTraveller, id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/BookingTravellers
        [HttpPost]
        public IActionResult PostBookingTraveller([FromBody] BookingTraveller bookingTraveller)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.AddTraveller(bookingTraveller);

            return CreatedAtAction("GetBookingTraveller", new { id = bookingTraveller.BookingTravellerId }, bookingTraveller);
        }
    }
}

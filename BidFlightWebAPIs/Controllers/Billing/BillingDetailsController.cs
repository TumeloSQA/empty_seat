﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Cors;

namespace BidFlightWebAPIs.Controllers.BillingDetail
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class BillingDetailsController : ControllerBase
    {
        private readonly IBillingDetailsRepository _billingDetailsRepository;

        public BillingDetailsController(IBillingDetailsRepository billingDetailsRepository)
        {
            _billingDetailsRepository = billingDetailsRepository;
        }

        // GET: api/BillingDetails
        [HttpGet]
        public IEnumerable<BillingDetails> GetBillingDetails()
        {
            return _billingDetailsRepository.GetDetails();
        }

        // GET: api/BillingDetails/5
        [HttpGet("{id}")]
        public IActionResult GetBillingDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var billingDetails = _billingDetailsRepository.GetBilling(id);

            if (billingDetails.Count() == 0)
            {
                return NotFound();
            }

            return Ok(billingDetails);
        }

        // PUT: api/BillingDetails/5
        [HttpPut("{id}")]
        public IActionResult PutBillingDetails([FromRoute] int id, [FromBody] BillingDetails billingDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != billingDetails.BillingId)
            {
                return BadRequest();
            }            

            try
            {
                _billingDetailsRepository.UpdateDetails(billingDetails,id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/BillingDetails
        [HttpPost]
        public IActionResult PostBillingDetails([FromBody] BillingDetails billingDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _billingDetailsRepository.AddBillingDetails(billingDetails);

            return CreatedAtAction("GetBillingDetails", new { id = billingDetails.BillingId }, billingDetails);
        }

        // DELETE: api/BillingDetails/5
        [HttpDelete("{id}")]
        public IActionResult DeleteBillingDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_billingDetailsRepository.DeleteBilling(id))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ClientModule.Helpers;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.CustomerAuthentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
//using System.Security.Claims;

namespace BidFlightWebAPIs.Controllers.BookingTravellerAuth
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingTravellerAuthController : ControllerBase
    {
        private readonly ICustomerAuthRepository _repository;

        private readonly PriceLineDbContext _context;

        private readonly AppSettings _appSettings;

        public BookingTravellerAuthController(ICustomerAuthRepository repository, PriceLineDbContext context, IOptions<AppSettings> appSettings)
        {
            _repository = repository;
            _context = context;
            _appSettings = appSettings.Value;
        }

        // GET: api/LocationManagement
        [HttpGet]
        public IEnumerable<Users> GetBookingTravellers()
        {
            return _repository.GetBookingTravellers();
        }

        [AllowAnonymous]
        [HttpPost, Route("Login")]
        public IActionResult Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return BadRequest("Username or Password is invalid");
            }

            Users user = _context.Users.SingleOrDefault(x => x.Username == username && x.Password == password && x.RoleId.Equals(1));

            // check if username exists
            if (user == null)
            {
                return Unauthorized();
            }
            else
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5000",
                    audience: "http://localhost:5000",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddHours(24),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new { Token = tokenString });

            }
        }

        //POST: api/LocationManagement
        [HttpPost]
        public IActionResult RegisterBookingUser([FromBody] BookingUserViewModel bookingUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.RegisterBookingUser(bookingUserViewModel);

            return CreatedAtAction("GetBookingTravellers", new { fullNames = bookingUserViewModel.FullNames }, bookingUserViewModel);
        }

        [HttpPut("{userID}")]
        public IActionResult Update([FromRoute]int userID, [FromBody]BookingUserViewModel bookingUserViewModel)
        {
            try
            {
                // save 
                _repository.Update(bookingUserViewModel, userID, bookingUserViewModel.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
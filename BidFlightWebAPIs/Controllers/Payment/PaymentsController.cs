﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using Microsoft.AspNetCore.Cors;

namespace BidFlightWebAPIs.Controllers.Payment
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentsRepository _paymentsRepository;

        public PaymentController(IPaymentsRepository paymentsRepository)
        {
            _paymentsRepository = paymentsRepository;
        }

        // GET: api/Payment
        [HttpGet]
        public IEnumerable<Payments> GetPayments()
        {
            return _paymentsRepository.GetPayments();
        }

        // GET: api/Payment/5
        [HttpGet("{id}")]
        public IActionResult GetPayments([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var payments = _paymentsRepository.GetPayment(id);

            if (payments.Count() == 0)
            {
                return NotFound();
            }

            return Ok(payments);
        }

        // PUT: api/Payment/5
        [HttpPut("{id}")]
        public IActionResult PutPayments([FromRoute] int id, [FromBody] Payments payments)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != payments.PaymentId)
            {
                return BadRequest();
            }

            try
            {
                _paymentsRepository.UpdateDetails(payments, id);
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/Payment
        [HttpPost]
        public IActionResult PostPayments([FromBody] Payments payments)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _paymentsRepository.AddBillingDetails(payments);

            return CreatedAtAction("GetPayments", new { id = payments.PaymentId }, payments);
        }

        // DELETE: api/Payment/5
        [HttpDelete("{id}")]
        public IActionResult DeletePayments([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_paymentsRepository.DeletePayment(id))
            {
                return NotFound();
            }

            return Ok();
        }      
    }
}
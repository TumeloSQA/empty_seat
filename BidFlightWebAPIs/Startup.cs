﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientModule.Helpers;
using ClientModule.Models;
using ClientModule.ServiceInterfaces.BidFlight;
using ClientModule.ServiceInterfaces.BookingTravellers;
using ClientModule.ServiceInterfaces.CustomerAuthentication;
using ClientModule.ServiceInterfaces.FlightsDetails;
using ClientModule.ServiceInterfaces.Locations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ServiceModules.ServiceImplementaion.BidFlight;
using ServiceModules.ServiceImplementaion.BookingTravellers;
using ServiceModules.ServiceImplementaion.CustomerAuthentication;
using ServiceModules.ServiceImplementaion.Locations;
using ServiceModules.ServiceImplementaion.FlightsDetails;
using Swashbuckle.AspNetCore.Swagger;

namespace BidFlightWebAPIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.TokenValidationParameters = new TokenValidationParameters
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,
                      ValidIssuer = "http://localhost:5000",
                      ValidAudience = "http://localhost:5000",
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"))
                  };
              });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddControllersAsServices()
                //JSON Reference Looping
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            //FrameworkServices

            services.AddDbContext<PriceLineDbContext>(options => options.UseSqlServer("Server=.;Database=PriceLineDb;Trusted_Connection=True;"));


            // Register the Swagger generator
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Empty Seat Web API Documentation", Version = "v1" });
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });

            services.AddScoped<ICustomerAuthRepository, CustomerAuthRepository>();
            services.AddScoped<ILocationManagementRepository, LocationManagementRepository>();
            services.AddScoped<IBookingTravellersRepository, BookingTravellersRepository>();
            services.AddScoped<IReservationRepository, ReservationRepository>();
            services.AddScoped<IBillingDetailsRepository, BillingDetailsRepository>();
            services.AddScoped<IPaymentsRepository, PaymentsRepository>();
            services.AddScoped<IFlightDetailsRepository, FlightDetailsRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Empty Seat Web API Documentation");
            });
        }
    }
}
